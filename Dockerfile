ARG CENTOS_VERSION=
ARG DOCKER_REGISTRY=
FROM ${DOCKER_REGISTRY}centos:${CENTOS_VERSION}

ARG JAVA_VERSION=

ARG USER_ID=9002
ARG GROUP_ID=9002

COPY scripts/* /home/javarun/bin/
COPY startup.sh /startup.sh

RUN groupadd -g ${GROUP_ID} javarun && \
    adduser -u ${USER_ID} -g ${GROUP_ID} -d /home/javarun --no-create-home javarun && \
    mkdir -p /home/javarun/{bin,lib,dumps} && \
    chown -R javarun:javarun /home/javarun  && \
    yum -y update && \
    yum -y install ${JAVA_VERSION}.x86_64 && \
    yum clean all && \
    rm -rf /var/cache/yum/x86_64/*/*/* && \
    chown javarun:javarun /etc/pki/ca-trust/extracted/java/cacerts && \
    chmod 750 /home/javarun/bin/add-ca-certs.sh /home/javarun/bin/set-java.sh && \
    chmod 644 /etc/pki/ca-trust/extracted/java/cacerts && \
    chmod 755 /startup.sh && \
    /home/javarun/bin/add-ca-certs.sh && \
    /home/javarun/bin/set-java.sh

ENV HOME=/home/javarun \
    JAVA_OPTS="-XX:+UseG1GC -Dsecurerandom.source=file:/dev/urandom"


WORKDIR /home/javarun

ENTRYPOINT ["/startup.sh"]

RUN set -xe; \
    chown -R javarun:root /home/javarun;    \
    chgrp -R 0 /home/javarun;            \
    chmod -R g+rw /home/javarun;         \
    find /home/javarun -type d -exec chmod g+x {} +

USER ${USER_ID}