pipeline {
  agent any
  environment {
    user_uid = "${env.JENKINS_UID}"
    user_gid = "${env.JENKINS_GID}"
    docker_registry = "${env.DOCKER_REGISTRY}"
    docker_registry_url = "${env.DOCKER_REGISTRY_URL}"
    docker_registry_credentials_id = "${env.DOCKER_REGISTRY_CREDENTIALS}"
    docker_image = "openjdk"
  }

  stages {
    stage('Initialize') {
      steps {
        checkout scm
      }
    }

    stage('Build and Publish Docker') {
      when {
        branch 'master'
      }
      steps {
          script {
            run_sh('8-jre-latest:java-1.8.0-openjdk-headless', '8')
            run_sh('11-jre-latest:java-11-openjdk-headless', '8')
            run_sh('17-jre-latest:java-17-openjdk-headless', '8')
          }
      }
    }

    stage ('Push alias tags') {
        when {
          branch 'master'
        }
        steps {
          script {
            docker.withRegistry(docker_registry_url, docker_registry_credentials_id) {
              img = docker.image("${docker_registry}/${docker_image}:8-jre-latest-centos-8")
              img.push("8-jre-latest")
              img.push("8-jre")
              img = docker.image("${docker_registry}/${docker_image}:11-jre-latest-centos-8")
              img.push("11-jre-latest")
              img.push("11-jre")
              img = docker.image("${docker_registry}/${docker_image}:17-jre-latest-centos-8")
              img.push("17-jre-latest")
              img.push("17-jre")
            }
          }
        }
      }
  }

  post {
    always{
      sendStatusEmail audience: "${JENKINS_NOTIFICATIONS_EMAIL}", changeLogSets: currentBuild.changeSets, buildStatus: currentBuild.currentResult
    }
  }
}

def run_sh(TAG, CENTOS) {
    def tag_split=TAG.split(':')
    app = docker.build("${docker_registry}/${docker_image}:latest" , " --pull=true --build-arg DOCKER_REGISTRY=${docker_registry}/ --build-arg CENTOS_VERSION=${CENTOS} --build-arg JAVA_VERSION=" + tag_split[1] + " --build-arg USER_ID=${user_uid} --build-arg GROUP_ID=${user_gid} ${env.WORKSPACE}")
    docker.withRegistry(docker_registry_url, docker_registry_credentials_id) {
      app.push(tag_split[0] + "-centos-" + CENTOS)
    }
}
