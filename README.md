# Devops area - Centos with OpenJDK


This is part of the devops.

It provides docker file that is setting up Centos with openjdk and jre runtine
## Prerequisites

* docker engine ce ( not the default docker of the linux distribution; look at docker site for install instructions), https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-from-a-package
* unix based OS. Tested on ubuntu 16.04 and Centos 7 ( might work on windows too )


## Getting Started


### Build it

From shell:
```
docker build --build-arg CENTOS_VERSION=7 . -t mycentos
```

Success looks like:
```
....
Running transaction test
Transaction test succeeded
Running transaction
  Installing : 14:libpcap-1.5.3-11.el7.x86_64                               1/2 
  Installing : 2:nmap-ncat-6.40-13.el7.x86_64                               2/2 
  Verifying  : 14:libpcap-1.5.3-11.el7.x86_64                               1/2 
  Verifying  : 2:nmap-ncat-6.40-13.el7.x86_64                               2/2 

Installed:
  nmap-ncat.x86_64 2:6.40-13.el7                                                

Dependency Installed:
  libpcap.x86_64 14:1.5.3-11.el7                                                

Complete!
Removing intermediate container f43ae78cb799
 ---> b9b975440377
Successfully built b9b975440377
Successfully tagged mycentos:latest
```
## Acknowledgments

* Devops tools is part of **Devops area** 
