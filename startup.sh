#!/bin/bash

set -e

SPRING_PROFILE_ARG=

if [[ -n $SPRING_PROFILE ]]; then
    SPRING_PROFILE_ARG="-Dspring.profiles.active=${SPRING_PROFILE}"
fi


if [[ -f /sys/fs/cgroup/memory/memory.limit_in_bytes ]]; then

    LC_NUMERIC="en_US.UTF-8"
    MAX_RAM_RAW=$(echo `expr $(cat /sys/fs/cgroup/memory/memory.limit_in_bytes) / 1048576 - 10`)
    MAX_RAM=$(printf "%.0f" $(echo "${MAX_RAM_RAW}" | bc))
    if [[ "$MAX_RAM" -lt "8796093022197" ]]; then # if equal or greater it means we are in an unconstrained environment; e.g. not inside docker

        JVM_MAX_HEAP=
        if [[ -z $JVM_RATIO ]]; then
            JVM_MAX_HEAP=$(printf "%.0f" $(echo "${MAX_RAM}" | bc))
        else
            JVM_MAX_HEAP=$(printf "%.0f" $(echo "${MAX_RAM} * ${JVM_RATIO}" | bc)) # we introduce a possibility to use ratio of the max memory ( e.g. 0.1 up to 1 )
        fi

        if [[ "$JVM_MAX_HEAP" -lt "1000" ]]; then # small java memmory space, remove G1 gc as it is not suitable for small ram allocations
            JAVA_OPTS="${JAVA_OPTS/-XX:+UseG1GC/}"
        fi
        JAVA_OPTS="$JAVA_OPTS -Xmx${JVM_MAX_HEAP}M"
    fi
fi

START_FILE="$@"

if [[ -f "$@"-${VERSION}.jar ]]; then
    START_FILE="$@"-${VERSION}.jar
elif [[ -f "$@"-${VERSION}.war ]]; then 
    START_FILE="$@"-${VERSION}.war
fi

exec java $JAVA_OPTS $SPRING_PROFILE_ARG -jar $START_FILE --server.port=8080 $JAVA_ARGS
