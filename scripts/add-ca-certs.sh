#!/usr/bin/env sh

CA_DIR=${1:-/etc/pki/ca-trust/source/anchors}
KEYSTORE_PASS=${2:-changeit}
KEYSTORE_FILE=${3:-/etc/alternatives/jre/lib/security/cacerts}

find ${CA_DIR} -type f \( -iname \*.crt -o -iname \*.pem \) -exec echo "{}" \; | while read PEM_FILE_PATH; do
    PEM_FILE=${PEM_FILE_PATH##*/}

    echo "[info] processing file ${PEM_FILE} ..."

    echo -n "[debug] " && head -n 3 ${PEM_FILE_PATH} && \
          echo "..." && tail -n 3 ${PEM_FILE_PATH}

    CERTS=$(grep 'END CERTIFICATE' ${PEM_FILE_PATH} | wc -l)
    echo "[info] - found ${CERTS} certs in file ${PEM_FILE}"

    for N in $(seq 0 $(($CERTS - 1))); do
          ALIAS="${PEM_FILE%.*}-$N"
          echo "[info] - adding with alias ${ALIAS} ..."
          cat ${PEM_FILE_PATH} | \
                awk "n==$N { print }; /END CERTIFICATE/ { n++ }" | \
                keytool -noprompt -import -trustcacerts \
                    -alias ${ALIAS} -keystore ${KEYSTORE_FILE} \
                    -storepass ${KEYSTORE_PASS} || \
                    (echo "[warn] - ${ALIAS} could not be added for file ${PEM_FILE}" && true)
    done

done
