#!/usr/bin/env bash

if [ "$(id -u)" != "0" ]; then
   echo "[error] this script must be run as root" 1>&2
   exit 1
fi

printf "export JAVA_HOME=/etc/alternatives/jre_openjdk/\nexport PATH=$PATH:$JAVA_HOME/bin" | tee /etc/profile.d/java.sh >/dev/null
hash -r

. /etc/profile.d/java.sh
